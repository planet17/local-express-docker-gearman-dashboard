# Local Express: Gearman Dashboard #

https://bitbucket.org/planet17/local-express-docker-gearman-dashboard

Separated third party container for watching queue state for applications by Test Specification.
That application is part of a few docker containers.
For more details about whole project look the repository
[Bridge](https://bitbucket.org/planet17/local-express-docker-core-bridge).



## Environment ##


-------------------

    Docker: *
    PHP >= 5.6
    Node: 4.0
    Apache: 2.4.25

-------------------


## Installation ##

After cloning that project create local `.env`

```shell
docker-compose up -d
```

`.docker/images/gearman-ui/config.uml` - use that config for set up observable services
